"use strict";
exports.__esModule = true;
//geting value of screen from html
var screen = document.querySelector("#screen");
//selecting buttons to perform actions and check which is selected
var btn = document.querySelectorAll(".btn");
//checking if Number or symbol
for (var _i = 0, btn_1 = btn; _i < btn_1.length; _i++) {
    var item = btn_1[_i];
    item.addEventListener("click", function (e) {
        var btntext = e.target.innerText;
        switch (btntext) {
            case "×":
                btntext = "*";
                break;
            case "÷":
                btntext = "/";
                break;
            case "mod":
                btntext = "%";
                break;
        }
        screen.value += btntext;
    });
}
//for deleting the number
var backspace = function () {
    screen.value = screen.value.substring(0, screen.value.length - 1);
};
//for power of 2 to the number
var pow = function () {
    screen.value = Math.pow(screen.value, 2);
};
// for finding the squre root of number
var sqrt = function () {
    screen.value = Math.sqrt(screen.value);
};
//for finding Exponential function of number
var exp = function () {
    screen.value = Math.exp(screen.value);
};
//multiplying with PI value
var pi = function () {
    if (screen.value == 0) {
        screen.value = 3.14;
    }
    else {
        screen.value = screen.value * Math.PI;
    }
};
//Log of 10 the value
var log = function () {
    screen.value = Math.log10(screen.value);
};
//Log of the value
var ln = function () {
    screen.value = Math.log(screen.value);
};
//Factorial value
var fact = function () {
    var i, num, f;
    if (screen.value == 0) {
        f = 0;
    }
    else {
        f = 1;
    }
    num = screen.value;
    for (i = 1; i <= num; i++) {
        f = f * i;
    }
    i = i - 1;
    screen.value = f;
};
//Division by 1
var divide = function () {
    screen.value = 1 / screen.value;
};
//10 raise to power calculation
var powx = function () {
    screen.value = Math.pow(10, screen.value);
};
//neagtive to positive and positive to neagtive
var negpos = function () {
    if (screen.value == 0) {
        screen.value = "-";
    }
    else {
        screen.value = screen.value * -1;
    }
};
// calculating with e value
var e = function () {
    if (screen.value == 0) {
        screen.value = Math.E;
    }
    else {
        screen.value = screen.value * Math.E;
    }
};
//finding sin
var sin = function () {
    screen.value = Math.sin(screen.value);
};
//finding cos
var cos = function () {
    screen.value = Math.cos(screen.value);
};
//finding tan
var tan = function (n) {
    screen.value = Math.tan(screen.value);
};
//finding cot
var cot = function () {
    screen.value = 1 / Math.tan(screen.value);
};
//Adding to memory
var memory = function (opration) {
    var temp = document.getElementById("memory");
    var memory = temp.value;
    console.log(memory);
    screen.value = eval(screen.value);
    if (isNaN(screen.value) != false) {
        screen.value = " Error";
    }
    else {
        switch (opration) {
            case "+":
                screen.value = parseInt(memory.value) + parseInt(screen.value);
                break;
            case "-":
                screen.value = parseInt(memory.value) - parseInt(screen.value);
                break;
        }
        memory.value = screen.value;
    }
};
// for Exponential
var fe = function () {
    var num = parseInt(screen.value);
    screen.value = num.toExponential();
};
// 2nd(Inverse Trigo) Implemented
var change = function () {
    var state = document.getElementById("snd");
    switch (state.value) {
        case "0":
            document.getElementById("dd1").style.display = "none";
            document.getElementById("dd2").style.display = "";
            state.value = "1";
            break;
        case "1":
            document.getElementById("dd1").style.display = "";
            document.getElementById("dd2").style.display = "none";
            state.value = "0";
            break;
    }
};
var handleClick = function (arg0, handleClick) {
    throw new Error('Function not implemented.');
};
