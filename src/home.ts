//importing from types.d.ts
import {Screen} from './types'
//geting value of screen from html
let screen: Screen = document.querySelector("#screen");
//selecting buttons to perform actions and check which is selected
let btn = document.querySelectorAll(".btn");

//checking if Number or symbol
for (let item of btn) {
  item.addEventListener("click", (e: any) => {
    let btntext:string = e.target.innerText;
    switch (btntext) {
      case "×":
         btntext = "*";
        break;
      case "÷":
         btntext = "/";
        break;
      case "mod":
         btntext = "%";
        break;
    }
    screen.value += btntext;
  });
}
//for deleting the number
let backspace = ():void =>  {
  screen.value = screen.value.substring(0, screen.value.length - 1);
}
//for power of 2 to the number
let pow = ():void => {
  screen.value = Math.pow(screen.value, 2);
}
// for finding the squre root of number
let sqrt = ():void => {
  screen.value = Math.sqrt(screen.value);
}
//for finding Exponential function of number
let exp = ():void => {
  screen.value = Math.exp(screen.value);
}
//multiplying with PI value
let pi = ():void => {
  if (screen.value == 0) {
    screen.value = 3.14;
  } else {
    screen.value = screen.value * Math.PI;
  }
}
//Log of 10 the value
let log = ():void => {
  screen.value = Math.log10(screen.value);
}
//Log of the value
let ln = ():void => {
  screen.value = Math.log(screen.value);
}
//Factorial value
let fact = ():void => {
  var i: number, num: number, f: number;
  if (screen.value == 0) {
    f = 0;
  } else {
    f = 1;
  }
  num = screen.value;
  for (i = 1; i <= num; i++) {
    f = f * i;
  }
  i = i - 1;

  screen.value = f;
}
//Division by 1
let divide = ():void => {
  screen.value = 1 / screen.value;
}
//10 raise to power calculation
let powx = ():void => {
  screen.value = 10 ** screen.value;
}
//neagtive to positive and positive to neagtive
let negpos = ():void => {
  if (screen.value == 0) {
    screen.value = "-";
  } else {
    screen.value = screen.value * -1;
  }
}
// calculating with e value
let e = ():void => {
  if (screen.value == 0) {
    screen.value = Math.E;
  } else {
    screen.value = screen.value * Math.E;
  }
}
//finding sin
let sin = ():void => {
  screen.value = Math.sin(screen.value);
}
//finding cos
let cos = ():void => {
  screen.value = Math.cos(screen.value);
}
//finding tan
let tan = (n: number):void => {
  screen.value = Math.tan(screen.value);
}
//finding cot
let cot = ():void => {
  screen.value = 1 / Math.tan(screen.value);
}
//Adding to memory
let memory = (opration: any):void => {
  let temp:any = document.getElementById("memory");
  let memory:any =temp.value;
  console.log(memory);
  screen.value = eval(screen.value);
  if (isNaN(screen.value) != false) {
    screen.value = " Error";
  } else {
    switch (opration) {
      case "+":
        screen.value = parseInt(memory.value) + parseInt(screen.value);
        break;
      case "-":
        screen.value = parseInt(memory.value) - parseInt(screen.value);
        break;
    }
    memory.value = screen.value;
  }
}

// for Exponential
let fe = ():void => {
  var num = parseInt(screen.value);
  screen.value = num.toExponential();
}

// 2nd(Inverse Trigo) Implemented
let change = ():void => {
  var state:any = document.getElementById("snd");


  switch (state.value) {
    case "0":
      document.getElementById("dd1").style.display = "none";
      document.getElementById("dd2").style.display = "";
      state.value = "1";
     
      break;

    case "1":
      document.getElementById("dd1").style.display = "";
      document.getElementById("dd2").style.display = "none";
     
      state.value = "0";
      break;
  }
}
let handleClick = (arg0: string, handleClick: any) => {
  throw new Error('Function not implemented.');
}

