 //autobind
 function AutoBind(_: any, _2: string, discriptor: PropertyDescriptor){
    const originalMethod = discriptor.value;
    const adjDescriptor: PropertyDescriptor = {
        configurable: true,
       get() {
          const boundFn = originalMethod.bind(this)
          return boundFn;
        }
      };
      return adjDescriptor
}
 
 //Input classs
 class ProjectInput{
    templateElement: HTMLTemplateElement;
    hostElement: HTMLDivElement;
    element : HTMLFormElement;
    titleInput: HTMLInputElement;
    discriptionInput: HTMLInputElement;
    peopleInput: HTMLInputElement;

    constructor(){
        this.templateElement= document.getElementById('project-input')! as HTMLTemplateElement;
        this.hostElement= document.getElementById('app')! as HTMLDivElement;

        const importedNode = document.importNode(this.templateElement.content, true)

        this.element = importedNode.firstElementChild as HTMLFormElement;
        this.element.id = 'user-input'
        this.titleInput = this.element.querySelector('#title') as HTMLInputElement;
        this.discriptionInput = this.element.querySelector('#description') as HTMLInputElement
        this.peopleInput = this.element.querySelector('#people') as HTMLInputElement
        console.log(this.titleInput,this.discriptionInput,this.peopleInput);
        
        this.configure();
        this.attach();
    }

    private gatherUserInput():[string,string,number] | void{
        const enteredTitle= this.titleInput.value;
        const enteredDescription=  this.discriptionInput.value;
        const enteredPeople=  this.peopleInput.value;
        console.log( enteredTitle, enteredDescription, +enteredPeople);
        
        if(
            enteredTitle.trim().length === 0 ||
            enteredDescription.trim().length === 0 ||
            enteredPeople.trim().length === 0
        ){
            alert('Invalide Input, Please Try Again!');
            return;
        }else{
            return [ enteredTitle,enteredDescription, +enteredPeople ]
        }
    }

    @AutoBind
    private submitHandler(event: Event){
        event.preventDefault();
        const userInput = this.gatherUserInput
        if( Array.isArray(userInput) ){
            const [title, desr, people] = userInput
            console.log([title, desr, people]);
            
        }
        
    }

    private configure(){
        this.element.addEventListener('submit',this.submitHandler)
    }

    private attach(){
        this.hostElement.insertAdjacentElement('afterbegin', this.element)
    }
 }
 const projInput = new ProjectInput() 