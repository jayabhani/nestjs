const names: Array<string> =['max','Nish']


function merge<T extends object, U extends object>(objA: T, objB: U){
    return Object.assign(objA, objB)
}

const mergerObj = merge({name:'max', hobbies: ['Sports']},{age:30})

console.log(mergerObj)

interface Lengthy{
    length: number;
}

function countAndPrint<T extends Lengthy>(elment: T):[T, string]{
    let descriptionText = 'Got no value';
    if(elment.length === 1){
        descriptionText = 'Got 1 Element'
    }
    else if(elment.length > 1){
        descriptionText = 'Got '+ elment.length +' elements';
    }
    return [elment, descriptionText]
}

console.log(countAndPrint('Hi T here!'))