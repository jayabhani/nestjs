var nsmes = ['max', 'Nish'];
function merge(objA, objB) {
    return Object.assign(objA, objB);
}
var mergerObj = merge({ name: 'max', hobbies: ['Sports'] }, { age: 30 });
console.log(mergerObj);
function countAndPrint(elment) {
    var descriptionText = 'Got no value';
    if (elment.length === 1) {
        descriptionText = 'Got 1 Element';
    }
    else if (elment.length > 1) {
        descriptionText = 'Got ' + elment.length + ' elements';
    }
    return [elment, descriptionText];
}
console.log(countAndPrint('Hi There!'));
