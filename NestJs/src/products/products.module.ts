import { Module } from '@nestjs/common';


import { ProductsConroller } from './products.controller';
import { ProductService } from './products.service';


@Module({
    controllers: [ProductsConroller],
    providers: [ProductService],
})

export class ProductsModule {}