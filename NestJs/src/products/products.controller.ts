import { Controller, Post, Body, Get, Param, Patch,Delete } from "@nestjs/common";
import { ProductService } from "./products.service";

@Controller('products')
export class ProductsConroller {
    constructor(private readonly productService: ProductService){}

    @Post()
    addproduct(@Body('title') prodTitle: string,
    @Body('description') prodDesc: string,
    @Body('price') prodPrice: number,) {
      const gentratedId =this.productService.insertProduct(prodTitle, prodDesc, prodPrice)

      return {id: gentratedId}
    }

    @Get()
    getAllProducts(){
      return this.productService.getProducts();
    }

    @Get(':id')
    getProduct(@Param('id') proID : string){
      return this.productService.getSingleProduct(proID)
    }

    @Patch(':id')
    getUpdate(
    @Param('id') proID : string ,
    @Body('title') prodTitle: string,
    @Body('description') prodDesc: string,
    @Body('price') prodPrice: number,)
    {
        this.productService.updateProduct(proID, prodTitle, prodDesc, prodPrice)
        return null
    }

    @Delete(':id')
    removeProduct(@Param('id') proID : string){
      this.productService.deleteProduct(proID)
      return {product: "Deleted"}
    }
}