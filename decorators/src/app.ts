function Logger(loadString: string){
    return function (constructor: Function){
        console.log(loadString);
        console.log(constructor);
    }
}
function WithTemplate(template:string, hookID:string){
    return function(constructor:any){
        const hookEl = document.getElementById(hookID)
        const p = new constructor();
        if(hookEl){
            hookEl.innerHTML = template;
            hookEl.querySelector('h1')!.textContent = p.name;
        }
    }
}

@Logger('Logging Person')
@WithTemplate('<h1>My Person Object</h1>','app')
class Person{
    name = 'max';

    constructor(){
        console.log('Creating person object...');
    }
}

const per = new Person();

console.log(per);

// ---
function Log(target: any, propertyName: string | Symbol){
    console.log("Property Decorator");
    console.log(target, propertyName);
    
}

function Log2(target: any, name: string, discriptor: PropertyDescriptor){
    console.log('Accsessor Decorater');
    console.log(target);
    console.log(name);
    console.log(discriptor);
}

function Log3(target: any, name: string, discriptor: PropertyDescriptor){
    console.log('Method Decorater');
    console.log(target);
    console.log(name);
    console.log(discriptor);
}
function Log4(target: any, name: string | Symbol,position: number ){
    console.log('Parameter Decorater');
    console.log(target);
    console.log(name);
    console.log(position);
}

class Product{
    @Log
    title : string;
    private _price: number;

    @Log2
    set price(val: number){
        if(val>0){
            this._price = val;
        }else{
            throw new Error("Invalid Price");
            
        }
    }
    constructor(t: string, p: number){
        this.title= t;
        this._price=p;
    }
    @Log3
    getPriceWithTax(@Log4 tax: number){
        return this._price*(1+tax)
    }
}

function AutoBind(_: any, _2: string, discriptor: PropertyDescriptor){
    const originalMethod = discriptor.value;
    const adjDescriptor: PropertyDescriptor = {
        configurable: true,
        enumerable: false,
       get() {
          const boundFn = originalMethod.bind(this)
          return boundFn;
        }
      };
      return adjDescriptor
}


class Printer {
    message= 'This Works!';

    @AutoBind
    showMessage(){
        console.log(this.message);
        
    }
}

    const p = new Printer();

    const button = document.querySelector('button')!;
    button.addEventListener('click',p.showMessage)
